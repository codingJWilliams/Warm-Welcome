
from minio import Minio
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
from PIL import ImageOps
from PIL import ImageFilter
from io import StringIO, BytesIO
import os, json, time, asyncio, requests

minioClient = Minio('10.0.200.130:8333', access_key='n/a', secret_key='n/a', secure=False)

def default(params, key, default):
  if key in params: return params[key]
  return default

def main(params):
  timer = time.time()
  _name = params["name"]
  _guild = params["guild"]
  _avatar = params["avatar"]
  _background = params["background"]
  
  _font = "./f.ttf"
  # Download font if parameter passed
  fMod = False
  if "font" in params:
    fMod = not fMod
    r = requests.get(params['font'], stream=True)
    with open("/tmp/f.ttf", 'wb') as f:
      for chunk in r.iter_content(chunk_size=1024): 
        if chunk:
          f.write(chunk)
    _font = "/tmp/f.ttf"

  new_width= int(default(params, "new_width", 1000))
  offset_x= int(default(params, "offset_x", 0))
  offset_y= int(default(params, "offset_y", -70))
  new_height= int(default(params, "new_height", 500))
  ava_sqdim= int(default(params, "ava_sqdim", 260))
  text_offset_x= int(default(params, "text_offset_x", 0))
  boxopacity = int(default(params, "boxopacity", 30))
  text_offset_y= int(default(params, "text_offset_y", 140))
  is_square= bool(default(params, "is_square", False))
  blur_radius= int(default(params, "blur_radius", 15))
  blur_offset_y= int(default(params, "blur_offset_y", 0))
  outline= bool(default(params, "outline", True))
  fontsize = int(default(params, "fontsize", 100))


  print("Setup variables: " + str(str(time.time() - timer)))
  timer = time.time()
  # Load in default font if font not modified, else load in the font from wherever we downloaded it from
  font = ImageFont.truetype('f.ttf' if not fMod else "/tmp/f.ttf", fontsize)

  print("Loaded font: " + str(str(time.time() - timer)))
  timer = time.time()

  # Download background image
  r = requests.get(_background)
  bg_im = BytesIO(r.content)
  im = Image.open(bg_im).convert("RGBA")
  print("Loaded image: " + str(str(time.time() - timer)))
  timer = time.time()
  width, height = im.size

  # Set welcome to the welcome message
  welcome = 'Welcome {0},\n to {1}!'.format(_name, _guild)
  if "welcome" in params:
    welcome = params["welcome"]

  # Calculate dimensions
  left = (width - new_width) // 2
  top = (height - new_height) // 2
  right = (width + new_width) // 2
  bottom = (height + new_height) // 2
  print("Done divisions: " + str(str(time.time() - timer)))
  timer = time.time()

  # Crop image to half the original size
  im = im.crop((left, top, right, bottom)).convert("RGB")
  print("Cropped image: " + str(str(time.time() - timer)))
  timer = time.time()
  ov_left = 0
  ov_top = (im.height // 2) + (blur_offset_y)
  ov_right = im.width
  ov_bottom = im.height
  ov_box = (ov_left, ov_top, ov_right, ov_bottom)
  img_center_x = (im.width // 2)
  img_center_y = (im.height // 2)
  print("More vars: " + str(str(time.time() - timer)))
  timer = time.time()

  # Box that goes across bottom
  ov_ic = im.crop(ov_box)
  ov_ic = ov_ic.filter(ImageFilter.GaussianBlur(blur_radius))
  print("Gaussian blur: " + str(str(time.time() - timer)))
  timer = time.time()

  # Put the blurred box over the top of the image
  im.paste(ov_ic, ov_box)

  # Prepare to draw text
  draw = ImageDraw.Draw(im, mode='RGBA')
  print("Created ImageDraw: " + str(str(time.time() - timer)))
  timer = time.time()
  draw.rectangle(((ov_left, ov_top), (ov_right, ov_bottom)), fill=(0, 0, 0, boxopacity))
  print("Drawn rectangle: " + str(str(time.time() - timer)))
  timer = time.time()
  url = _avatar
  print(url)

  # Download avatar
  r = requests.get(url)
  avatar_im = BytesIO(r.content)
  resize = (ava_sqdim, ava_sqdim)
  avatar_im = Image.open(avatar_im).convert("RGBA")
  avatar_im = avatar_im.resize(resize, Image.ANTIALIAS)
  avatar_im.putalpha(avatar_im.split()[3])
  print("Got and opened avatar: " + str(str(time.time() - timer)))

  # Possibly mask avatar
  timer = time.time()
  if not is_square:
    mask = Image.new('L', resize, 0)
    maskDraw = ImageDraw.Draw(mask)
    maskDraw.ellipse((0, 0) + resize, fill=255)
    mask = mask.resize(avatar_im.size, Image.ANTIALIAS)
    avatar_im.putalpha(mask)
  print("Loaded mask for avatar: " + str(str(time.time() - timer)))
  timer = time.time()
  img_offset_x = img_center_x + offset_x
  img_offset_y = img_center_y + offset_y
  ava_right = img_offset_x + avatar_im.width//2
  ava_bottom = img_offset_y + avatar_im.height//2
  ava_left = img_offset_x - avatar_im.width//2
  ava_top = img_offset_y - avatar_im.height//2
  text_width, text_height = draw.textsize(welcome, font=font)
  x = ((img_center_x - text_width / 2) + text_offset_x)
  y = ((img_center_y - text_height / 2) + text_offset_y)
  im.paste(avatar_im, box=(ava_left, ava_top, ava_right, ava_bottom), mask=avatar_im)
  print("Inserted avatar: " + str(str(time.time() - timer)))
  timer = time.time()
  draw.text((x, y), welcome, fill=(255, 255, 255, 255), font=font, align='center')
  print("Drew text: " + str(str(time.time() - timer)))

  # Write to a file, upload to "s3", then delete.
  filename = str(int(time.time())) + ".png"
  im.save("/tmp/" + filename)
  try:
    minioClient.fput_object('welcome-images', filename, "/tmp/" + filename)
  except ResponseError as err:
    print(err)
  os.remove("/tmp/" + filename)

  return {
    "statusCode": 200,
    "headers": {
      "Content-Type": "application/json"
    },
    "body": json.dumps({ "url": "https://obj.voidcrafted.me/" + filename })
  }