const commando = require("discord.js-commando");

module.exports = class UserInfoCommand extends commando.Command {
	constructor(client) {
		super(client, {
            name: 'test',
            group: "welcomer",
            memberName: 'test',
            userPermissions: ["MANAGE_SERVER"],
			description: 'Triggers a test welcome in an identical way to how a real welcome would be triggered.',
			guildOnly: true
		});
	}

	async run(msg, args) {
        await require("../../doWelcome")( msg.member )
    }
};